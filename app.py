import os
import flask
from flask import request, jsonify

app = flask.Flask(__name__)
app.config["DEBUG"] = True

APP_PORT   = int(os.getenv("APP_PORT", 8080))
APP_SECRET = os.getenv("MY_SUPER_SECRET")


@app.route('/', methods=['GET'])
def hello_world():
    return jsonify({
        "hello": "world"
    }), 200

@app.route('/healthcheck', methods=['GET'])
def healthcheck():
    if not APP_SECRET:
        return jsonify({
            "status": 500,
            "health": "NOT OK",
            "reason": "MY_SUPER_SECRET secret missing"
        }), 500

    return jsonify({
        "status": 200,
        "health": "OK"
    }), 200

@app.route('/readiness', methods=['GET'])
def readiness():
    return jsonify({
        "status": 200,
        "ready": "OK"
    }), 200

app.run(host='0.0.0.0', port=APP_PORT)
